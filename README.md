# Api Fencing



### Sommaire :

1. [Présentation du projet](#présentation-du-projet)
2. [Technologies Utilisées](#technologies-utilisées)
3. [Intérets](#intérets)
4. [Installation et utilisation](#installation-et-utilisation)
5. [Points forts](#points-forts)
6. [Pour la v2](#pour-la-v2)
7. [Diagramme de classes](#diagramme-de-classes)
8. [Aperçu](#aperçu)
9. [License](#license)



### <span name="présentation-du-projet">Présentation du projet</span>

__Master Piece__<br>
Dernier projet individuel, au choix, de la formation Simplon.

>Création d'une API regroupant des escrimeurs(se) français(e), leur équipe ainsi que leurs stats afin de créer un site inspiré de celui de la Fédération Internationale d'Escrime <a href="https://fie.org/">FIE</a>. 

_Temps : 1 mois_



## <span name="technologies-utilisées">Technologies utilisées</span>

* Symfony 4 



## <span name="intérets">Intérets</span>

Se familiariser à la création d'API REST et à manipuler des entités avec relations.



## <span name="installation-et-utilisation">Installation et utilisation</span>

1. __Cloner le projet__
2. Installer les dépendances avec __composer install__
3. Créer un fichier .env.local avec dedans DATABASE_URL=mysql://__user__:__password__@127.0.0.1:3306/__db_name__ en remplaçant user, password et db_name par vos informations de connexion à votre base de données.
4. Faire un bin/console doctrine:migrations:migrate pour mettre la bdd dans le bon état (si jamais cela ne marche pas, tenter de faire un bin/console doctrine:schema:drop --force avant de refaire le migrate pour remettre à zéro la bdd).

Le projet dispose de fixtures chargeable avec doctrine : __bin/console doctrine:fixtures:load__



## <span name="points-forts">Points forts</span>

* Départ d'API intéressant



## <span name="pour-la-v2">Pour la v2</span>

- Ajouter :
    - D'autres champions
    - D'autres équipes



## <span name="diagramme-de-classes">Diagramme de classes</span>

<img src="public/images/class-diagram.jpg" alt="Diagramme des classes">



## <span name="aperçu">Aperçu</span>

_Petit aperçu de l'API_

<img src="public/images/api-fencing.png" alt="Une partie de l'API">



## <span name="license">License</span>

Le projet et son code peuvent être utilisés sans limitations.